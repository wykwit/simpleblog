> simpleblog:
> The simplest blog software, crafted hastily with django.

Unsurprisingly, it took me only a couple of minutes tops to write this.
Surprisingly, I wrote it around the end of 2018 and it served me well for a whole year, until the end of the decade.
The _production version_ differed only by a settings.py file.
