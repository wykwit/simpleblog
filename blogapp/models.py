from django.db import models

from datetime import date

class Post(models.Model):
    title = models.CharField(max_length=64, default="unnamed post")
    content = models.TextField(default="")
    pin = models.BooleanField(default=False)
    date = models.DateField(default=date.today)

    def __str__(self):
        return self.title
