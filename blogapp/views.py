from django.shortcuts import render

from .models import Post

def display_home(request):
    posts = Post.objects.order_by('-pin', '-date', '-id')
    return render(request, 'blog.html', {'posts': posts})

def display_post(request, post_id):
    posts = Post.objects.filter(id=post_id)
    return render(request, 'post.html', {'posts': posts})
