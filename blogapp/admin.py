from django.contrib import admin

from .models import Post

class PostAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'date', 'pin', 'id')

admin.site.register(Post, PostAdmin)
